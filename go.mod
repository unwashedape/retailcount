module gitlab.com/unwashedape/retailcount

go 1.16

require (
	github.com/Boostport/migration v0.21.2
	github.com/alecthomas/kong v0.2.17
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-chi/cors v1.2.0
	github.com/google/uuid v1.3.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/plaid/plaid-go v0.1.0-beta.1.0.20210804014804-6e614f244d88
)

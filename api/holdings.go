package api

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/plaid/plaid-go"
)

type Holding struct {
	Account    string  `json:"account"`
	AccountID  string  `json:"account_id"`
	SecurityID string  `json:"security_id"`
	Ticker     string  `json:"ticker"`
	Quantity   float32 `json:"quantity"`
}

func PlaidHoldings(client *plaid.APIClient) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get("X-Access-Token")
		if accessToken == "" {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		ctx := r.Context()
		res, _res, err := client.PlaidApi.InvestmentsHoldingsGet(ctx).
			InvestmentsHoldingsGetRequest(*plaid.NewInvestmentsHoldingsGetRequest(accessToken)).
			Execute()
		if err != nil {
			log.Printf("err %v", err)
			if _res != nil {
				out, _ := httputil.DumpResponse(_res, true)
				log.Println(string(out))
			}
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.NewEncoder(rw).Encode(map[string]interface{}{
			"holdings": holdings(res),
		})
		if err != nil {
			log.Printf("err %v", err)
		}
	}
}

// holdings returns a formatted slice of holdings from the plaid get response.
// The plaid response has accounts, holdings, and securities split out into
// three arrays. This is nice to deduplicate data, but makes working with the
// data annoying on the front end. This method formats the response in a
// slightly nicer format.
func holdings(plaidHoldings plaid.InvestmentsHoldingsGetResponse) (holdings []Holding) {
	holdings = make([]Holding, 0, len(plaidHoldings.Holdings))
	isEquity := func(s plaid.Security) bool {
		t := s.Type.Get()
		return t != nil && *t == "equity"
	}
	accts, securities := map[string]string{}, map[string]string{}
	for _, acct := range plaidHoldings.Accounts {
		accts[acct.AccountId] = acct.Name
	}
	for _, sec := range plaidHoldings.Securities {
		if isEquity(sec) && sec.TickerSymbol.IsSet() {
			securities[sec.SecurityId] = *sec.TickerSymbol.Get()
		}
	}

	for _, holding := range plaidHoldings.Holdings {
		ticker, ok := securities[holding.SecurityId]
		if !ok {
			continue
		}
		acctName, ok := accts[holding.AccountId]
		if !ok {
			continue
		}
		holdings = append(holdings, Holding{
			AccountID:  holding.AccountId,
			SecurityID: holding.SecurityId,
			Account:    acctName,
			Ticker:     ticker,
			Quantity:   holding.Quantity,
		})
	}
	return holdings
}

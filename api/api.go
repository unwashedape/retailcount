package api

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/google/uuid"
	"github.com/plaid/plaid-go"
)

type PlaidOptions struct {
	ClientID   string `flag:"" name:"plaid.clientid" help:"" env:"PLAID_CLIENT_ID"`
	Secret     string `flag:"" name:"plaid.secret" help:"" env:"PLAID_SECRET"`
	Env        string `flag:"" name:"plaid.env" help:"" env:"PLAID_ENV"`
	ClientName string `flag:"" name:"plaid.clientname" help:"" env:"PLAID_CLIENT_NAME" default:"Retail Count"`
	Language   string `flag:"" name:"plaid.language" help:"" env:"PLAID_LANGUAGE" default:"en"`
}

func (po PlaidOptions) Client() *plaid.APIClient {
	configuration := plaid.NewConfiguration()
	configuration.AddDefaultHeader("PLAID-CLIENT-ID", po.ClientID)
	configuration.AddDefaultHeader("PLAID-SECRET", po.Secret)
	env := plaid.Sandbox
	switch po.Env {
	case "sandbox", "sb":
		env = plaid.Sandbox
	case "production", "prod":
		env = plaid.Production
	case "development", "dev":
		env = plaid.Development
	}
	configuration.UseEnvironment(env)
	return plaid.NewAPIClient(configuration)
}

func LinkTokenCreate(client *plaid.APIClient, opts PlaidOptions) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		createReq := plaid.NewLinkTokenCreateRequest(
			opts.ClientName, opts.Language, []plaid.CountryCode{plaid.COUNTRYCODE_US}, plaid.LinkTokenCreateRequestUser{
				ClientUserId: uuid.NewString(), // only used for log searching
			},
		)
		products := []plaid.Products{plaid.PRODUCTS_INVESTMENTS}
		createReq.Products = &products
		ctx := r.Context()
		res, _res, err := client.PlaidApi.LinkTokenCreate(ctx).LinkTokenCreateRequest(*createReq).Execute()
		if err != nil {
			log.Printf("err %v", err)
			if _res != nil {
				out, _ := httputil.DumpResponse(_res, true)
				log.Println(string(out))
			}
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(rw).Encode(map[string]string{
			"link_token": res.LinkToken,
		})
		if err != nil {
			log.Printf("err %v", err)
		}
	}
}

func PublicTokenExchange(client *plaid.APIClient) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		publicToken := r.Header.Get("X-Public-Token")
		if publicToken == "" {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		res, _res, err := client.PlaidApi.ItemPublicTokenExchange(ctx).
			ItemPublicTokenExchangeRequest(
				*plaid.NewItemPublicTokenExchangeRequest(publicToken),
			).
			Execute()
		if err != nil {
			log.Printf("err %v", err)
			if _res != nil {
				out, _ := httputil.DumpResponse(_res, true)
				log.Println(string(out))
			}
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.NewEncoder(rw).Encode(map[string]string{
			"access_token": res.AccessToken,
		})
		if err != nil {
			log.Printf("err %v", err)
		}
	}
}

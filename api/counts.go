package api

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/go-chi/chi/v5"
	"github.com/plaid/plaid-go"
	"gitlab.com/unwashedape/retailcount/store"
)

func AddCounts(countStore store.CountStorage, client *plaid.APIClient) http.HandlerFunc {
	type countRequest struct {
		AccountID  string `json:"account_id"`
		SecurityID string `json:"security_id"`
	}
	return func(rw http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get("X-Access-Token")
		if accessToken == "" {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		var req countRequest
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Printf("Failed parsing count req %v", err)
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		ctx := r.Context()
		res, _res, err := client.PlaidApi.InvestmentsHoldingsGet(ctx).
			InvestmentsHoldingsGetRequest(*plaid.NewInvestmentsHoldingsGetRequest(accessToken)).
			Execute()
		if err != nil {
			log.Printf("err %v", err)
			if _res != nil {
				out, _ := httputil.DumpResponse(_res, true)
				log.Println(string(out))
			}
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		indexedHoldings := map[store.CountKey]Holding{}
		for _, h := range holdings(res) {
			indexedHoldings[store.CountKey{
				AccountID:  h.AccountID,
				SecurityID: h.SecurityID,
			}] = h
		}

		key := store.CountKey{
			AccountID:  req.AccountID,
			SecurityID: req.SecurityID,
		}
		if holding, ok := indexedHoldings[key]; !ok {
			rw.WriteHeader(http.StatusBadRequest)
			return
		} else {
			countStore.Add(key, holding.Ticker, float32(holding.Quantity))
		}
		rw.WriteHeader(http.StatusAccepted)
	}
}

func TickerCounts(countStore store.CountStorage) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		tickerCounts, err := countStore.GetAll()
		if err != nil {
			log.Printf("Failed getting tickers %v", err)
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(rw).Encode(map[string][]store.TickerCount{
			"tickers": tickerCounts,
		})
		if err != nil {
			log.Printf("encoding tickers %v", err)
		}
	}
}

func TickerCount(store store.CountStorage) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ticker := chi.URLParam(r, "ticker")
		if ticker == "" {
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		count, exists, err := store.Get(ticker)
		if err != nil {
			log.Printf("Failed getting tickers %v", err)
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
		if !exists {
			rw.WriteHeader(http.StatusNotFound)
			return
		}

		err = json.NewEncoder(rw).Encode(map[string]float32{
			"count": count,
		})
		if err != nil {
			log.Printf("encoding tickers %v", err)
		}
	}
}

package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/alecthomas/kong"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"

	"gitlab.com/unwashedape/retailcount/api"
	"gitlab.com/unwashedape/retailcount/store"
	"gitlab.com/unwashedape/retailcount/web"
)

var cli struct {
	api.PlaidOptions
	Serve ServeCmd `cmd:"" default:"1" help:"Start server."`
}

func main() {
	ctx := kong.Parse(&cli)
	ctx.FatalIfErrorf(ctx.Run(cli.PlaidOptions))
}

type ServeCmd struct {
	Addr  string `help:"Address to listen on." default:"0.0.0.0:8080" env:"LISTEN_ADDR"`
	Store string `help:"Backing store to use." default:"mem" env:"STORE"`
	DSN   string `help:"DSN to use for backing store." env:"STORE_DSN"`
}

func (cmd *ServeCmd) Run(options api.PlaidOptions) error {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"}, // DANGEROUS
		AllowedMethods:   []string{"OPTIONS", "GET", "POST", "PUT", "DELETE"},
		AllowedHeaders:   []string{"Content-Type", "Content-Length", "Accept-Encoding", "accept", "origin", "Cache-Control", "X-Requested-With", "X-Access-Token", "X-Public-Token"},
		AllowCredentials: true,
	}))
	var (
		s   store.CountStorage
		err error
	)
	switch cmd.Store {
	case "mem":
		s = store.NewMem()
	case "pg":
		s, err = store.NewPG(cmd.DSN)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("unknown store %q", cmd.Store)
	}
	client := options.Client()
	r.Post("/api/link_token/create", api.LinkTokenCreate(client, options))
	r.Post("/api/public_token/exchange", api.PublicTokenExchange(client))
	r.Get("/api/holdings", api.PlaidHoldings(client))
	r.Put("/api/tickers/count", api.AddCounts(s, client))
	r.Get("/api/tickers", api.TickerCounts(s))
	r.Get("/api/tickers/{ticker}/count", api.TickerCount(s))
	r.NotFound(web.AssetHandler)
	log.Printf("Listening on %s", cmd.Addr)
	return http.ListenAndServe(cmd.Addr, r)
}

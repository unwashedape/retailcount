FROM node:16 as SPA
WORKDIR /usr/src/web

COPY web/package.json ./
COPY web/yarn.lock ./
RUN yarn install

COPY web ./

RUN yarn build

FROM golang:alpine as API
WORKDIR /usr/src/api
ADD go.* ./
RUN go mod download
ADD . ./
COPY --from=SPA /usr/src/web/build ./web/build
RUN go build -o /build/bin/server ./cmd/server


FROM alpine
COPY --from=API /build/bin/server .
EXPOSE 8080
ENTRYPOINT ["./server"]
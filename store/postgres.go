package store

import (
	"context"
	"embed"
	"time"

	"github.com/Boostport/migration"
	"github.com/Boostport/migration/driver/postgres"
	"github.com/jackc/pgx/v4"
)

// Create migration source
//go:embed postgres/migrations
var embedFS embed.FS

var embedSource = &migration.EmbedMigrationSource{
	EmbedFS: embedFS,
	Dir:     "postgres/migrations",
}

func NewPG(dsn string) (CountStorage, error) {
	conn, err := pgx.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}
	s := &pgstore{dsn, conn}
	if err := s.migrate(); err != nil {
		return nil, err
	}
	return s, nil
}

type pgstore struct {
	dsn  string
	conn *pgx.Conn
}

func (pg pgstore) Add(key CountKey, ticker string, count float32) error {
	_, err := pg.conn.Exec(context.Background(), `
	INSERT INTO ticker_counts (account_id, security_id, ticker, count)
	VALUES ($1, $2, $3, $4)
	ON CONFLICT (account_id, security_id) DO UPDATE
	SET count = $4, counted_at = $5
`, key.AccountID, key.SecurityID, ticker, count, time.Now())
	if err != nil {
		return err
	}
	return nil
}
func (pg pgstore) Get(ticker string) (count float32, exists bool, err error) {
	err = pg.conn.QueryRow(context.Background(), `
	SELECT sum(count) FROM ticker_counts WHERE ticker = $1 GROUP BY ticker)
`, ticker).Scan(&count)
	if err != nil {
		return 0, false, err
	}
	return count, true, nil
}
func (pg pgstore) GetAll() (tickerCounts []TickerCount, err error) {
	rows, err := pg.conn.Query(context.Background(), "SELECT ticker, sum(count) FROM ticker_counts GROUP BY ticker")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tickerCounts = make([]TickerCount, 0)
	for rows.Next() {
		var tc TickerCount
		if err := rows.Scan(&tc.Symbol, &tc.Count); err != nil {
			return nil, err
		}
		tickerCounts = append(tickerCounts, tc)
	}
	return tickerCounts, nil
}

func (pg pgstore) migrate() error {
	driver, err := postgres.New(pg.dsn)
	if err != nil {
		return err
	}
	defer driver.Close()
	_, err = migration.Migrate(driver, embedSource, migration.Up, 0)
	if err != nil {
		return err
	}
	return nil
}

package store

import (
	"sync"
)

type CountStorage interface {
	Add(key CountKey, ticker string, count float32) error
	Get(ticker string) (count float32, exists bool, err error)
	GetAll() ([]TickerCount, error)
}

type CountKey struct {
	AccountID  string
	SecurityID string
}

type TickerCount struct {
	Symbol string  `json:"symbol"`
	Count  float32 `json:"count"`
}

func NewMem() CountStorage {
	return &memstore{
		tickers: map[string]float32{},
		counts:  map[CountKey]float32{},
	}
}

type memstore struct {
	mu      sync.RWMutex
	tickers map[string]float32
	counts  map[CountKey]float32
}

func (ms *memstore) Add(key CountKey, ticker string, count float32) error {
	ms.mu.Lock()
	defer ms.mu.Unlock()
	tickerCount := ms.tickers[ticker]
	if previousCount, ok := ms.counts[key]; ok {
		tickerCount -= previousCount
	}
	ms.tickers[ticker] = tickerCount + count
	ms.counts[key] = count
	return nil
}

func (ms *memstore) Get(ticker string) (count float32, exists bool, err error) {
	ms.mu.RLock()
	defer ms.mu.RUnlock()
	count, exists = ms.tickers[ticker]
	return count, exists, nil
}

func (ms *memstore) GetAll() ([]TickerCount, error) {
	counts := make([]TickerCount, 0, len(ms.tickers))
	ms.mu.RLock()
	defer ms.mu.RUnlock()
	for symbol, count := range ms.tickers {
		counts = append(counts, TickerCount{symbol, count})
	}
	return counts, nil
}

CREATE TABLE IF NOT EXISTS ticker_counts(
    account_id TEXT NOT NULL,
    security_id TEXT NOT NULL,
    count NUMERIC NOT NULL,
    ticker TEXT NOT NULL,
    counted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(account_id, security_id)
);
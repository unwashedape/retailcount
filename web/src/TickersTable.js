import React, { useState, useCallback, useEffect } from "react";
import axios from "axios";

const tickersURL = `${process.env.REACT_APP_API || ""}/api/tickers`;

const TickersTable = ({ accessToken }) => {
  const [loading, setLoading] = useState(true);
  const [tickers, setTickers] = useState([]);
  const fetchToken = useCallback(async () => {
    const { data } = await axios({
      method: "get",
      url: tickersURL,
      headers: {
        "X-Access-Token": accessToken,
      },
    });
    setTickers(data.tickers);
    setLoading(false);
  }, [accessToken, setTickers]);

  useEffect(() => {
    fetchToken();
  }, [fetchToken]);

  if (loading) {
    return (
      <div className="row">
        <small>Loading...</small>
      </div>
    );
  }

  return (
    <table className="u-full-width">
      <thead>
        <tr>
          <th>Ticker</th>
          <th>Count</th>
        </tr>
      </thead>
      <tbody>
        {tickers.map((ticker, idx) => (
          <tr key={idx}>
            <td>{ticker.symbol}</td>
            <td>{ticker.count}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TickersTable;

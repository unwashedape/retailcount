import React, { useState, useCallback, useEffect } from "react";
import { usePlaidLink } from "react-plaid-link";
import axios from "axios";

const linkTokenURL = `${process.env.REACT_APP_API || ""}/api/link_token/create`;
const exchangeTokenURL = `${
  process.env.REACT_APP_API || ""
}/api/public_token/exchange`;

const PlaidLink = ({ setAccessToken }) => {
  const [linkToken, setLinkToken] = useState("");

  const fetchToken = useCallback(async () => {
    const { data } = await axios({
      method: "post",
      url: linkTokenURL,
    });
    setLinkToken(data.link_token);
  }, []);

  useEffect(() => {
    fetchToken();
  }, [fetchToken]);

  const exchangeToken = useCallback(
    async (token) => {
      try {
        const { data } = await axios({
          method: "post",
          url: exchangeTokenURL,
          headers: {
            "X-Public-Token": token,
          },
        });
        setAccessToken(data.access_token);
      } catch (error) {
        console.error(error);
      }
    },
    [setAccessToken]
  );

  const { open, ready, err } = usePlaidLink({
    token: linkToken,
    onSuccess: exchangeToken,
  });

  const warnBeforeOpen = useCallback(() => {
    // eslint-disable-next-line no-restricted-globals
    const confirmed = confirm(
      `THIS IS A SANDBOX ENVIRONMENT. USE user_good / pass_good - DO NOT USE REAL CREDENTIALS!`
    );
    if (!confirmed) {
      return;
    }
    open();
  }, [open]);

  if (err) return <p>Error!</p>;

  return (
    <div>
      <button
        className="button-primary"
        onClick={warnBeforeOpen}
        disabled={!ready}
      >
        Link an investment account
      </button>
    </div>
  );
};

export default PlaidLink;

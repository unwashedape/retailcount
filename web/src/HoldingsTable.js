import React, { useState, useCallback, useEffect } from "react";
import axios from "axios";

const holdingsURL = `${process.env.REACT_APP_API || ""}/api/holdings`;
const countURL = `${process.env.REACT_APP_API || ""}/api/tickers/count`;

const HoldingsTable = ({ accessToken }) => {
  const [loading, setLoading] = useState(true);
  const [holdings, setHoldings] = useState([]);
  const fetchToken = useCallback(async () => {
    const { data } = await axios({
      method: "get",
      url: holdingsURL,
      headers: {
        "X-Access-Token": accessToken,
      },
    });
    setHoldings(data.holdings);
    setLoading(false);
  }, [accessToken, setHoldings]);

  const countHolding = useCallback(
    async (holding) => {
      await axios({
        method: "put",
        url: countURL,
        headers: {
          "X-Access-Token": accessToken,
        },
        data: {
          account_id: holding.account_id,
          security_id: holding.security_id,
        },
      });
    },
    [accessToken]
  );

  useEffect(() => {
    fetchToken();
  }, [fetchToken]);

  if (loading) {
    return (
      <div className="row">
        <small>Loading...</small>
      </div>
    );
  }

  return (
    <table className="u-full-width">
      <thead>
        <tr>
          <th>Account</th>
          <th>Ticker</th>
          <th>Quantity</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {holdings.map((holding, idx) => (
          <tr key={idx}>
            <td>{holding.account}</td>
            <td>{holding.ticker}</td>
            <td>{holding.quantity}</td>
            <td style={{ width: "100px" }}>
              <button onClick={() => countHolding(holding)}>Count</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default HoldingsTable;

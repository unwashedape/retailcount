import React, { useState } from "react";
import PlaidLink from "./PlaidLink";
import HoldingsTable from "./HoldingsTable";
import TickersTable from "./TickersTable";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  NavLink,
} from "react-router-dom";

function App() {
  const [accessToken, setAccessToken] = useState("");
  return (
    <Router>
      <Redirect from="/" to="/tickers" />
      <div className="container">
        <div className="row" style={{ marginTop: "15%" }}>
          <div className="two-thirds column">
            <h4>Retail Count</h4>
            <p>
              Unofficial counting of how many shares are held by retail
              investors.
            </p>
          </div>
        </div>
        <nav className="navbar">
          <div className="container">
            <ul className="navbar-list">
              <li className="navbar-item">
                <NavLink
                  className="navbar-link"
                  activeClassName="active"
                  to="/tickers"
                >
                  Tickers
                </NavLink>
              </li>
              <li className="navbar-item">
                <NavLink
                  className="navbar-link"
                  activeClassName="active"
                  to="/link"
                >
                  Link Account
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
        <Switch>
          <Route path="/tickers">
            <TickersTable />
          </Route>
          <Route path="/link">
            {!accessToken && (
              <div className="row">
                <PlaidLink setAccessToken={setAccessToken} />
              </div>
            )}
            {accessToken && (
              <div className="row">
                <HoldingsTable accessToken={accessToken} />
              </div>
            )}
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;

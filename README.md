# Retail Count

Unofficial counting of how many shares are held by retail investors.

![Demo](.docs/assets/demo.gif)

[Sandbox Demo](https://spring-snowflake-103.fly.dev/)
